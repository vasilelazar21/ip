public class Micro {
    public static void main(String[] args)
    {
        /*args = new String[2];
        args[0] = "17";
        args[1] = "2";
        */

        //Calculam pretul pe o anumita distanta, danda ca parametri distanta si pretul/km
        //Se poate rula cu jar
        if(args.length < 2)
        {
            System.out.println("Argumente lipsa");
            return;
        }

        float km, pret, total;
        try
        {
            km = Float.parseFloat(args[0]);
            pret = Float.parseFloat(args[1]);
        }
        catch(NumberFormatException e)
        {
            System.out.println("Argumentele nu sunt numere");
            return;
        }

        System.out.println("Catre destinatie sunt "+km+" km!");
        total=km*pret;
        System.out.println("Pretul total este: "+total+" lei");


    }
}
